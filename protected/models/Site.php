<?php
class Site extends CFormModel {

	public $name;
	public $pass;
	public $email;
	public $date;
	public $gender;
	public $description;
	public $city;
	
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
				// name, email, subject and body are required
				array('name, email, pass, date', 'required'),
				// email has to be a valid email address
				array('email', 'email'),
		);
	}
	
	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
				'name' => 'Name',
				'pass' => 'Password',
				'email' => 'E-mail',
				'date' => 'Date',
		);
	}
	
}