<?php
$this->pageTitle = 'Gerenciar';
$this->breadcrumbs = array('Gerenciar');

EQuickDlgs::iframeButton (
		array (
				'controllerRoute' => '/site/create',
				'actionParams' => array (),
				'dialogTitle' => 'Create item',
				'dialogWidth' => '50%',
				/*'dialogHeight' => 600,*/
				'contentWrapperHtmlOptions' => array (
						'style' => 'height: 100%;'
				),
				'iframeHtmlOptions' => array (
						'style' => 'border: 0px none'
				),
				'openButtonText' => 'Create new',
				'openButtonHtmlOptions' => array (
						'class' => 'btn btn-primary'
				),
				'dialogAttributes' => array (
						'options' => array (
								'show' => array (
										'effect' => 'fade',
										'duration' => 400
								),
								'hide' => array (
										'effect' => 'fade',
										'duration' => 400
								),
								'buttons' => array (
										array (
												'text' => 'Fechar',
												'class' => 'btn btn-primary',
												'click' => 'js:function() {
                            $( this ).dialog( "close" );
                        }'
										)
								)
						)
				),
				// 'closeButtonText' => 'Close',
				'closeOnAction' => true,
				// 'refreshGridId' => 'group-grid', //the grid with this id will be refreshed after closing
		) // important to invoke the close action in the actionCreate
);

