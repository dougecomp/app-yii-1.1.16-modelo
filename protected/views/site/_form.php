<?php

/** @var TbActiveForm $form */
$form = $this->beginWidget(
	'booster.widgets.TbActiveForm',
	array(
			'id' => 'verticalForm',
			//'type' => 'horizontal',
			'htmlOptions' => array(/* 'class' => '' */), // for inset effect
	)
);

echo $form->textFieldGroup($model, 'name');
echo $form->passwordFieldGroup($model, 'pass');
echo $form->emailFieldGroup($model, 'email');
echo $form->datePickerGroup($model, 'date', array(
		'widgetOptions' => array(
			'options' => array(
				'format' => 'dd/mm/yyyy',
				'language' => 'pt'
			),
		),
		'prepend'=>'<i class="fa fa-calendar"></i>',
	)
);
echo $form->textAreaGroup($model, 'description');
echo $form->select2Group($model, 'city',
	array(
		/* 'wrapperHtmlOptions' => array(
			'class' => 'col-sm-5',
		), */
		'widgetOptions' => array(
			'asDropDownList' => false,
			'options' => array(
				/* 'tags' => array(1 => 'Feira de Santana',2 => 'Salvador'), */
				/* 'data' => array(
					array('id'=>1, 'text'=>'Feira de Santana'),
					array('id'=>2, 'text'=>'Salvador'),
				), */
				'ajaxUrl' => Yii::app()->createAbsoluteUrl('site/listCities'),
				'refetchUrl' => Yii::app()->createAbsoluteUrl('site/refetchCity'),
				'placeholder' => 'Select a City',
				'width' => '25%',
				/* 'tokenSeparators' => array(',', ' ') */
			),
			'events' => array(
				'change' => new CJavaScriptExpression('js:function(e) {
					toastr.info("Texto", "Título");
				}'),
			),
		),
	)
);
echo $form->radioButtonListGroup($model, 'gender', array(
		'widgetOptions' => array(
			'data' => array('M'=>'Masculino', 'F' => 'Feminino'),
		),
		//'inline' => true,
		
));
$this->widget(
	'booster.widgets.TbButton',
	array(
		'buttonType' => 'submit',
		'label' => 'Salvar',
		'context' => 'primary'
	)
);

$this->endWidget();
