<?php
$this->pageTitle = 'Cadastrar';
$this->breadcrumbs = array('Cadastrar');
?>

<div class="well form">
	<form action="" method="post">
		<div class="form-group">
			<input type="text" name="nome" class="form-control"></input>
		</div>
		<button type="submit" class="btn btn-primary">Cadastrar</button>
	</form>
</div>