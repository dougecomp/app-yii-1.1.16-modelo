<?php
$this->pageTitle = 'Seja Bem vindo!';
$this->breadcrumbs = array('');

EQuickDlgs::contentButton(
	array(
	    'content' => 'This is the help text', //$this->renderPartial('_help',array(),true),
	    'dialogTitle' => 'Default Dialog',
	    'dialogWidth' => '50%',
	    //'dialogHeight' => '50%',
	    'openButtonText' => 'Clique',
	    'openButtonHtmlOptions'=>array(
	    	'class'=>'btn btn-primary',
	    ),
	    'dialogAttributes' => array(
	    	'options' => array(
	    		'show' => array('effect'=>'fade','duration'=>400),
	    		'hide' => array('effect'=>'fade','duration'=>400),
		    	'buttons' => array(
	                array(
	                    'text'=>'Fechar',
	                    'class'=>'btn btn-primary',
	                    'click' =>'js:function() {
	                        $( this ).dialog( "close" );
	                    }',
	                ),
	            ),
	    	),
	    ),
	    //'closeButtonText' => 'Fechar', //comment to remove the close button from the dialog
	)
);