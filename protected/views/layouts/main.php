<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" >
<head>
	<meta charset="UTF-8">
    <?php
        $cs = Yii::app()->clientScript;
        $cs->registerCssFile($this->assetsBase.'/css/main.css');
        $cs->registerCssFile($this->assetsBase.'/fontawesome/css/font-awesome.min.css');
        $cs->registerScript('toggleMenu',
        '
        var menuWidth = 225;
        $("#toggleMenu").click( function (e) {
            if( $("#menu").width() > 200 ) {
                var toggleWidth = "60px";
                $(".nav > li > a > span").fadeToggle();
                $(".item-menu-full").toggleClass("item-menu-shrink");
                $(".item-menu-full").toggleClass("item-menu-full");
                $("#menu").animate({width: toggleWidth}, 1000, "swing", function(){
                });
            } else {
                var toggleWidth = menuWidth+"px";
                $(".item-menu-shrink").toggleClass("item-menu-full");
                $(".item-menu-shrink").toggleClass("item-menu-shrink");
                $("#menu").animate({width: toggleWidth}, 1000, "swing", function(){
                    $(".nav > li > a > span").fadeToggle();
                });
            }
            
        });
        ', CClientScript::POS_END);
    ?>
	<?php //require '_head.php'; ?>
    <title><?=$this->pageTitle?></title>
</head>

<body>

<?php

$this->widget('ext.Hzl.toastr.HzlToastr', array(
    'flashMessagesOnly' => true, //default to false.  True will fetch setFlashes data
    'options' => array(
        'timeOut' => 8000,
    )
));

?>


<div class="container-fluid" style="margin-top:2%;padding-left: 0px;" id="page">

	<div class="col-sm-4 col-md-2 well" style="padding-left: 5px; padding-right: 5px;" id="menu" >
        <!-- <span style="color: #428bca; ">MENU PRINCIPAL</span> -->
        <a id="toggleMenu"><i data-toggle="tooltip" data-placement="right" title="Ocultar/Revelar menu" class="fa fa-bars" style="cursor: pointer;"></i></a>
    	<?php
    	$this->widget(
    	   'booster.widgets.TbMenu',
    		array(
    			'type' => 'pills',
    			'stacked' => true,
                'activateParents' => true,
                'encodeLabel' => false,
    			'items' => array(
    				array('label' => '<span>Cliente</span>', 'url'=>'#', 'icon' => 'fa fa-home fa-lg','itemOptions'=>array('class'=>'')),
    				array('label' => '<span>item 2</span>', 'icon' => 'fa fa-group', 'itemOptions'=>array('class'=>''), 'submenuOptions'=>array('class'=>'item-menu-full'),'items' => array(
    					array('label'=>'subitem 2.1', 'icon'=>'fa fa-user', 'itemOptions'=>array('class'=>'')),
    					array('label'=>'subitem 2.2', 'icon' => 'fa fa-shopping-cart', 'itemOptions'=>array('class'=>''), 'items' => array(
    						array('label'=>'subsubitem 2.2.1', 'icon'=> 'fa fa-calendar'),
                            array('label'=>'subsubitem 2.2.1', 'icon'=> 'fa fa-car'),
                            array('label'=>'subsubitem 2.2.2', 'icon'=> 'fa fa-cloud', 'itemOptions'=>array('class'=>''), 'items' => array(
                                    array('label'=>'subsubitem 2.2.2.1', 'icon'=>'fa fa-comments'),
                                    array('label'=>'subsubitem 2.2.2.2', 'icon'=>'fa fa-desktop'),
                            )),
    					)),
    				)),
                    array('label' => '<span>item 2</span>', 'icon' => 'fa fa-group', 'itemOptions'=>array('class'=>''), 'submenuOptions'=>array('class'=>'item-menu-full'),'items' => array(
                        array('label'=>'subitem 2.1', 'icon'=>'fa fa-user', 'itemOptions'=>array('class'=>'')),
                        array('label'=>'subitem 2.2', 'icon' => 'fa fa-shopping-cart', 'itemOptions'=>array('class'=>''), 'items' => array(
                            array('label'=>'subsubitem 2.2.1', 'icon'=> 'fa fa-calendar'),
                            array('label'=>'subsubitem 2.2.1', 'icon'=> 'fa fa-car'),
                            array('label'=>'subsubitem 2.2.2', 'icon'=> 'fa fa-cloud', 'itemOptions'=>array('class'=>''), 'items' => array(
                                    array('label'=>'subsubitem 2.2.2.1', 'icon'=>'fa fa-comments'),
                                    array('label'=>'subsubitem 2.2.2.2', 'icon'=>'fa fa-desktop'),
                            )),
                        )),
                    )),
    			),
    		)
    	); 
    	?>
	</div>

    <div class="col-sm-7 col-md-10">
        <div class="well">
                <?php
                $this->widget(
                    'booster.widgets.TbBreadcrumbs',
                    array(
                        'links' => $this->breadcrumbs,
                        'htmlOptions' => array('class'=>'breadcrumb col-sm-12 col-md-12', 'style'=>'background-color:transparent; padding:0px'),
                    )
                );
                ?>
            <h1><?php echo $this->pageTitle?></h1>
            	<div id="content">
					<?php echo $content; ?>
				</div>
        </div>
    </div>

</div>

<div id="footer">
	Copyright &copy; <?php echo date('Y'); ?> by My Self<br/>
	Todos os Direitos Reservados<br/>
	<?php //echo Yii::powered(); ?>
</div><!-- footer -->

<?php //$this->endWidget(); ?>



</body>
</html>