<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class SiteController extends Controller
{
	/**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex() {
		
		//echo 'Hello World';
		$this->render('index');

	}

	public function actionAdmin() {

		$this->render('admin', array());

	}

	public function actionCreate() {

		if( isset($_POST['nome']) && !empty($_POST['nome'])) {

			$script = '';
			if($_POST['nome'] == 'erro') {
				$script = 'window.parent.toastr.error("Erro no Cadastro!", "Erro")';
			} else {
				$script = 'window.parent.toastr.success("Cadastro Realizado!", "Sucesso")';
			}
			//close the dialog and update the grid instead of redirect if called by the create-dialog
            EQuickDlgs::checkDialogJsScript($script);
            $this->redirect( array('admin') );
            
        }

		EQuickDlgs::render('create',array());

	}

	public function actionError() {
		
		$this->render('erro');

	}
	
	public function actionNew() {
		$model = new Site();
		
		$model->city = 1;
		
		if( isset($_POST['Site']) ) {
			$model->setAttributes($_POST['Site']);
		}
		
		$this->render('new', 
			array(
				'model'=>$model
			)
		);
		
	}
	
	public function actionListCities() {
		
		echo json_encode(
			array(
				array('id'=>1, 'text'=>'Feira de Santana'),
				array('id'=>2, 'text'=>'Salvador'),
			)
		);
		
	}
	
	public function actionRefetchCity() {
		$cities = array(
			array('id'=>1, 'text'=>'Feira de Santana'),
			array('id'=>2, 'text'=>'Salvador'),
		);
		
		foreach ($cities as $city) {
			if($city['id'] == $_POST['term']) {
				echo json_encode($city);
			}
		}
		
	}
}